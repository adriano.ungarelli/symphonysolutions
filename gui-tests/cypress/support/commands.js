Cypress.Commands.add('loginGUI', function() {
    cy.visit('https://www.saucedemo.com')
    
    cy.get('#user-name')
        .should('be.visible')
        .type(Cypress.env('username'), { log: false });
    cy.get('#password').type(Cypress.env('password'), { log: false });
    cy.get('#login-button').click();
})

Cypress.Commands.add('logoutGUI', function() {
    cy.get('#react-burger-menu-btn').click()
    cy.get('#logout_sidebar_link').click()
    cy.get('[data-test="login-button"]').should('be.visible')
})