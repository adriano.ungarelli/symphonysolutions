const { addMochawesomePlugin } = require('cypress-multi-reporters');

module.exports = (on, config) => {
    addMochawesomePlugin(on);
};