describe('Sauce Demo', () => {
  beforeEach(() => {
    cy.loginGUI()
  })

  afterEach(() => {
    cy.logoutGUI()
  })

  it('Verify that the items are sorted by Name A-Z', () => {
    cy.get('.title').should('have.text', 'Products')
    cy.get('select.product_sort_container').select('az')

    // Get all elements with class .inventory_item_name
    cy.get('.inventory_item_name').then(($elements) => {
      // Get the text of each element
      const itemNames = $elements.map((index, element) => Cypress.$(element).text()).get()

      // Check if the list of names is sorted in ascending order
      const sortedItemNames = [...itemNames].sort()
      expect(itemNames).to.deep.equal(sortedItemNames)
    });
  })

  it('Verify that the items are sorted by Name Z-A', () => {
    cy.get('.title').should('have.text', 'Products')
    cy.get('#root').screenshot('before order')
    cy.get('select.product_sort_container').select('za')
    cy.get('#root').screenshot('after order')
    // Get all elements with class .inventory_item_name
    cy.get('.inventory_item_name').then(($elements) => {
      // Get the text of each element
      const itemNames = $elements.map((index, element) => Cypress.$(element).text()).get();

      // Check if the list of names is sorted in ascending order
      const sortedItemNames = [...itemNames].sort().reverse()
      expect(itemNames).to.deep.equal(sortedItemNames)
    });
  })
})