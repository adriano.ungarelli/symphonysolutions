# Sauce Demo GUI tests

## Description
Test Automation for Sauce Demo GUI

## Before starting automation

### Indicate in .gitignore the directories and files that should not be subject to version control

  For exemple:
  ```
    .DS_Store
    gui-tests/node_modules/
    gui-tests/cypress/downloads/
    gui-tests/cypress/fixtures/
    gui-tests/cypress/screenshots/
    gui-tests/cypress/videos/
  ```
### Add environment variables file

Create the cypress.env.json file
To add:
```
{
     "username": "abc",
     "password": "123"
}
```
### To generate HTML reports of running Cypress we can use the "mochawesome" reporting library together with the "cypress-multi-reporters" plugin

1. Install required dependencies:
     npm install --save-dev mochawesome cypress-multi-reporters

2. Configure Cypress to use mochawesome reporter:
     In your cypress/plugins/index.js file, add the following code:
     ```
     const { addMochawesomePlugin } = require('cypress-multi-reporters');

     module.exports = (on, config) => {
       addMochawesomePlugin(on);
     };

     ```
3. Add the report generation script to your package.json file:
     ```
     "scripts": {
       "test": "cypress run",
       "report": "cypress run --reporter mochawesome"
     }

     ```
4. Run in headless mode:
    - npm run report
    - npx cypress run --spec "cypress\e2e\saucedemo.cy.js" --headless



